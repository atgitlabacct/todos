class Task < ActiveRecord::Base
  before_save :set_completed_date

  def uncomplete
    self.completed_date = ""
    self.completed = false
  end

  def completed?
    self.completed == true
  end

  private

  def set_completed_date
    if self.completed?
      self.completed_date = DateTime.now unless self.completed_date
    else
      self.completed_date = ""
    end
  end
end
