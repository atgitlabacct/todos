class Todos.Models.Tasks extends Backbone.Collection
  url: '/tasks'
  model: Todos.Models.Task

  incomplete: ->
    this.filter( (task) ->
      return task if not task.isCompleted()
    )

  completed: ->
    this.filter( (task) ->
      return task if task.isCompleted()
    )
