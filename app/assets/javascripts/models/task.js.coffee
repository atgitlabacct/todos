class Todos.Models.Task extends Backbone.Model

  defaults: {
    'name': 'New Task',
    'completed': false
  }

  validate: (attrs) ->
    mergedAttributes = _.extend(_.clone(@attributes), attrs)
    if ((mergedAttributes.name != undefined) && (mergedAttributes.name.trim() == ""))
      return "name can't be blank"

  name: ->
    @get('name')

  isCompleted: ->
    @get('completed')

  complete: ->
    @set({completed: true, completed_date: new Date().toLocaleString()})

  uncomplete: ->
    @set({completed: false, created_date: null})
