class Todos.Views.IncompleteTaskView extends Todos.Views.TaskView
  template: JST['incomplete_task_view']

  events:
    'click .task.done': 'makeComplete'
    'change .task.name': 'changeName'

  constructor: ->
    @events = _.extend( {}, Todos.Views.TaskView.prototype.events, this.events)
    Todos.Views.TaskView.prototype.constructor.apply( this, arguments )

  render: =>
    $(@el).html(@template(this.model.toJSON()))
    return this

  changeName: (element)->
    @model.set({name: $(element.target).val()})
    @model.save()

  makeComplete: () ->
    @model.complete()
    @model.save(@model.attributes)

  render: ->
    $(@el).html(@template(this.model.toJSON()))
    return this
