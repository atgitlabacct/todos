class Todos.Views.CompletedTasksView extends Backbone.View
  template: JST['tasks_view']

  render: ->
    $(@el).html(@template({description: "Completed tasks"}))
    new Todos.Models.Tasks(@collection).each((task) ->
      task_view = new Todos.Views.CompletedTaskView({model: task})
      this.$('table').append(task_view.render().el)
    , @)

    return this
