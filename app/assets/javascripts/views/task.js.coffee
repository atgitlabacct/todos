class Todos.Views.TaskView extends Backbone.View
  url: '/tasks'
  tagName: "tr"
  className: "task"

  events:
    'click .task.delete': 'destroy'

  initialize: ->
    this.model.bind('change', @render)

  render: =>
    $(@el).html(@template(this.model.toJSON()))
    return this

  destroy: ->
    @model.destroy()
