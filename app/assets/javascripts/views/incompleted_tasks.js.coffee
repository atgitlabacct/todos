class Todos.Views.IncompleteTasksView extends Backbone.View
  template: JST['tasks_view']

  render: ->
    $(@el).html(@template({description: "Incompleted tasks"}))
    new Todos.Models.Tasks(@collection).each((task) ->
      task_view = new Todos.Views.IncompleteTaskView({model: task})
      this.$('table').append(task_view.render().el)
    , @)

    return this
