class Todos.Views.NewTaskView extends Backbone.View
  events:
    'keypress input.name': 'addTask'

  initialize: ->
    @collection.bind('error', @.display_error, @)

  addTask: (event)->
    $name = this.$('input.name')
    if event.charCode == 13
      event.preventDefault()
      newAttributes = {name: $name.val()}
      errorCallback = {error: @displayError}
      if @collection.create(newAttributes, errorCallback)
        $name.val('')

  displayError: (model, error) ->
    $('.task.new .error').html(error)
    setTimeout("$('.task.new .error').html('')", 3000)

  render: ->
    this
