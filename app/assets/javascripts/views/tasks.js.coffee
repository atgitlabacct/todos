class Todos.Views.TasksView extends Backbone.View

  initialize: ->
    @collection.bind('add', @.render, @)
    @collection.bind('remove', @.render, @)
    @collection.bind('change', @.render, @)

  render: ->
    incomplete_tasks_view = new Todos.Views.IncompleteTasksView(
      {
        collection: @collection.incomplete(),
        el: $('.tasks .incomplete')
      })

    completed_tasks_view = new Todos.Views.CompletedTasksView(
      {
        collection: @collection.completed(),
        el: $('.tasks .completed')
      })

    incomplete_tasks_view.render()
    completed_tasks_view.render()

    return this
