class Todos.Views.ActionsView extends Backbone.View
  template: JST['actions_view']

  events:
    'click .action.task.add': 'add_task'
    'click .task.delete': 'render'

  initialize: ->
    @tasks = @options.tasks

  render: ->
    @el.html(@template())
    return this

  add_task: ->
    @tasks.create({name: 'New Task'})
