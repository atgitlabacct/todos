class Todos.Views.CompletedTaskView extends Todos.Views.TaskView
  template: JST['completed_task_view']

  constructor: ->
    @events = _.extend( {}, Todos.Views.TaskView.prototype.events, this.events)
    Todos.Views.TaskView.prototype.constructor.apply( this, arguments )

  events:
    'click .task.not-done': 'makeUncomplete'

  render: ->
    $(@el).html(@template(this.model.toJSON()))
    return this

  makeUncomplete: ->
    @model.uncomplete()
    @model.save()
