# This is a manifest file that'll be compiled into including all the files listed below.
# Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
# be included in the compiled file accessible from http://example.com/assets/application.js
# It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
# the compiled file.
#
#
#= require jquery
#= require jquery_ujs
#= require underscore-min
#= require backbone-min
#= require hamlcoffee
#= require namespaces
#= require_tree ../templates
#= require_tree ./models
#= require ./views/task
#= require_tree ./views
#= require_tree .
jQuery ->
  tasks_view = new Todos.Views.TasksView({collection: window.Tasks})
  tasks_view.render()

  actions_view = new Todos.Views.ActionsView({el: $('.topbar .container'), tasks: window.Tasks})
  actions_view.render()

  window.new_task_view = new Todos.Views.NewTaskView({collection: window.Tasks, el: $('.task.new')})
