describe "Task", ->
  beforeEach( ()->
    @task = new Todos.Models.Task({name: "some name"})
  )

  it "should have a name", ->
    expect(@task.name()).toEqual("some name")


  describe ".isCompleted", ->
    it "should be completed", ->
      @task = new Todos.Models.Task({name: "some name", completed: true})
      expect(@task.isCompleted()).toEqual(true)

    it "should not be completed", ->
      @task = new Todos.Models.Task({name: "some name", completed: false})
      expect(@task.isCompleted()).toEqual(false)

