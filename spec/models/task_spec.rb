require "spec_helper"

describe Task do
  before(:each) do
    @completed_task = Task.new(:name => 'Test Task', :completed => true)
  end

  context "when saving" do
    let(:fake_date) { DateTime.parse("10-10-10") }

    before(:each) do
      DateTime.stub!(:now).and_return "10-10-10"
    end

    context "and completed" do
      it "sets the completed date" do
        @completed_task.save
        @completed_task.completed_date.should eql(fake_date)
      end
    end

    context "and uncomplete" do
      it "then it clears the completed date" do
        @completed_task.uncomplete
        @completed_task.save
        @completed_task.completed_date.should be_nil
      end
    end
  end

  describe "#complete?" do
    it "returns true when complete" do
      @completed_task.completed?.should eql(true)
    end

    it "returns false when not complete" do
      Task.new(:completed => false).completed?.should eql(false)
    end
  end

  describe "#uncomplete" do
    it "sets the task to uncompleted" do
      @completed_task.uncomplete
      @completed_task.completed?.should eql(false)
    end

    it "sets the completed date to empty" do
      @completed_task.uncomplete
      @completed_task.completed_date.should be_nil
    end
  end
end
